//
//  OUVRE PORTE
//               /^
//       \\  A  //  
//        \\ _ //
//          | |   POIGNEE
//        -| - |- 
//          `-' 
//           B 


/* [Précisions] */

$fn=150;
tolerance = 0.4; // [0.4:0.4:2.0]

/* [Affichage] */
// afficher piece A
piece_a = true;

// afficher piece B
piece_b = true;

// simulation
simulation = true;
// Opacité
opacite = 1; // [0:0.1:1]


/* [Réglages pièces] */

// forme de la poignée
forme = "carrée"; // ["ronde","carrée"]

// Largeur de la poignée
largeur =13; // [11:22]

// Hauteur de la poignée
hauteur=26; //[11:50]

// epaisseur pièce B
epaisseur_b = 4; // [3:0.4:4]


//largeur piece B
largeur_b = 36.5; // [35:0.5:37]


// espacement entre pièces
espacement = 5; // [0.5:0.5:50]

/* [Fixations] */
// combo box for string
type_fixation="Parker"; // [Parker, M4]




// fixation
module fixation(complement_piece="") {
   //  translate([-15,-22,10])
    
   if (complement_piece == "") {

      // perçage b
      translate([largeur_b/2-4,espacement+9+epaisseur_b-0.5,10])
         rotate([90,0,0])
            cylinder(epaisseur_b+1,d=4.1);
      translate([-largeur_b/2+4,espacement+9+epaisseur_b-0.5,10])
         rotate([90,0,0])
            cylinder(epaisseur_b+1,d=4.1);
      
      // emplacement tête vis pièce b
      translate([largeur_b/2-4,espacement+8+epaisseur_b+11,10])
         rotate([90,0,0])
            if (type_fixation=="M4")
               cylinder(13.5,d=8.1,$fn=6);
            else
               cylinder(11,d=8);
      translate([-largeur_b/2+4,espacement+8+epaisseur_b+11,10])
         rotate([90,0,0])
            if (type_fixation=="M4")
               cylinder(13.3,d=8.1,$fn=6);
            else
               cylinder(11,d=8);


      // percage a
      translate([largeur_b/2-4,8,10])
         rotate([90,0,0])
            if (type_fixation=="M4")
               union(){
                  cylinder(22,d=4);
                  translate([0,0,10])
                     cylinder(12,d=8);
               }
            else
               cylinder(12,d=2.2);
      translate([-largeur_b/2+4,8,10])
         rotate([90,0,0])
            if (type_fixation=="M4")
               union(){
                  cylinder(22,d=4);
                  translate([0,0,10])
                     cylinder(12,d=8);
               }
            else
               cylinder(12,d=2.2);
      
      



   }
   if (complement_piece == "a")  {
      translate([largeur_b/2-4,6.65,10])
         rotate([90,0,0])
            color([1,0.5,0,opacite])
              cylinder(15,r=6);
      translate([-largeur_b/2+4,6.65,10])
         rotate([90,0,0])
            color([1,0.5,0,opacite])
               cylinder(15,r=6);
   }

   if (complement_piece == "b")  {
      translate([largeur_b/2-4,espacement+8+epaisseur_b,10])
         rotate([90,0,0])
           cylinder(epaisseur_b,r=6);
      translate([-largeur_b/2+4,espacement+8+epaisseur_b,10])
         rotate([90,0,0])
           cylinder(epaisseur_b,r=6);
   }


    
}

// logo
module logo() {

   translate([39,-27,-2])

   rotate([90,0,133])
      scale([0.25,0.25,0.25])
         linear_extrude(height=5,
                        convexity=10)
            import("logo_simpli.svg");
   
}


// PIECE A
// forme de base du support
module part_a() {
   translate([6,-21,0])
      scale([-0.65,-0.65,0.65])
         color([1,0.5,0,opacite])
            linear_extrude(height=30,convexity=15)
               import("mimi_makers67_base.svg",center=true,dpi=300);
}


// PIECE B
// forme de base du support
module part_b() {
   
   translate([0,espacement+8,0])
   {
      translate([-largeur_b/2,0,0])
      cube([largeur_b,epaisseur_b,20]);
      if (forme == "carrée") {
         taille_cube_b = epaisseur_b + hauteur  - espacement - 8;



         translate([-(largeur+epaisseur_b*2)/2,0,0])
         cube([largeur+epaisseur_b*2,
               taille_cube_b,
               20]);
      }
   }
      
   if (forme == "ronde") {
      difference() {
         translate([0,(hauteur+tolerance)/2,0])
            scale([largeur+tolerance+epaisseur_b*2,hauteur+tolerance+epaisseur_b*2,1])
               // forme cylindrique aux dimensions indiquées
               cylinder(h = 20, d=1);

               translate([-largeur_b/2,-(espacement+8),-0.5])
                  cube([largeur_b,(espacement+8)*2,21]);
      }
      
   }
}



// TODO:
// module mim_makers67_b()

module poignee() {

    color([0.7,0.7,0.7,0.7,opacite]) {
    // dans le cas d'une poignée carrée
    if (forme=="carrée") {
     translate([-(largeur+tolerance)/2,0,-50])
     // cube aux dimensions de la poignée
     cube([largeur+tolerance+0.1,hauteur+tolerance+0.1,100]);
        
    }
    
    // dans le cas d'une poignée ronde
    
    if (forme=="ronde") {
         translate([0,(largeur+tolerance)/2,-50])
            scale([largeur+tolerance,hauteur+tolerance,1])
        
         // forme cylindrique aux dimensions indiquées
         cylinder(h = 100, d=1);
    }
    }
}

// TODO: modules de fixation
// module fixation_parker
// module fixation_m4
// module logo

// CE MODULE DOIT ETRE APPELE POUR AVOIR UN EFFET
// VOIR PLUS BAS DANS EXEMPLE
module ouvreporte() {
   
    
    extension_dy_base = - 3.5;    
    extension_sy = largeur/4;

    
    
    poignee_position = [-largeur/2,-14-hauteur/2,-20];
    encombrement_position = [-(largeur+tolerance)/2,-14-(hauteur+tolerance)/2,-20];
    
   // PIECE A
   // On enleve la forme de la poignée à la forme de l'ouvre porte
   if (piece_a) {
      difference() {

      union() {
      part_a();
      fixation(complement_piece="a");


      }
         union() {
            // RETRAIT DE LA FORME DE LA POIGNEE
            //Selon le cas, on va retirer une forme carrée ou cylindrique
            // à la forme de base
            // couleur rouge pour voir la forme de la poignée
            

            // on place la forme de la poignée au bon endroit
            poignee();
               
            // RETRAIT PERçAGE
            fixation();
         
            
         }

      }
      translate([0.5,0.2,0])
      intersection() {
         part_a();
         logo();
      }
    }


    // PIECE B
    if (piece_b) {
      
      deplace_part_b = (!simulation) ? 10 : 0;
      
      translate([0,deplace_part_b,0]) 
      difference() {
         union() {
            color([1,0.8,0.2,opacite])
            part_b();
            fixation(complement_piece="b");
         }
         
         color([.8,0.5,0,opacite])
            union() {
               
               // RETRAIT DE LA FORME DE LA POIGNEE
               // on place la forme de la poignée au bon endroit
               poignee();

               // PERçAGE
               fixation();

            }
      }


    }
    // AFFICHAGE DE LA POIGNEE
    if (simulation==true) {
      poignee();
    }

}



// part_a();



// part_b(forme=forme,
//               largeur=largeur,
//               hauteur=hauteur,
//               epaisseur_b=epaisseur_b,
//               largeur_b=largeur_b);


// EXEMPLES

// POIGNEE RONDE
ouvreporte();


