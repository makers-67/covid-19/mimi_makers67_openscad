# mimi_makers67_openscad

Version paramétrable (Openscad) du modèle Mimi Makers67

## Prérequis

* installer [openscad](https://www.openscad.org/downloads.html)
* Dans openscad décocher _Vue>Hide customizer_
* installer [git](https://git-scm.com/)
   * pour windows, vous pouvez utiliser [tortoise git](https://tortoisegit.org/) pour avoir une intégration dans les [menus contextuels](https://tortoisegit.org/docs/tortoisegit/tgit-dug.html#tgit-dug-general-menu)


## Usage

### Général

* Ouvrir le fichier [mimi_makers67.scad](mimi_makers67.scad) avec openscad
* La pièce de base est générée à partir de l'image svg [mimi_makers67_base.svg](mimi_makers67_base.svg)
* Sur la droit les paramètres de réglage sont affichés

![](img/01_vue_ensemble.png)

| Précisions |  |
|---------|------------|
| $fn     | Précision du modèle |
| tolerance | tolérance entre la poignée et la pièce |

| Affichage |  |
|---------|------------|
| piece a | afficher la pièce A |
| piece b | afficher la pièce B |
| simulation | Met la pièce en situation avec la poignée. Décochée la pièce est affichée pour pouvoir être imprimée |

| Précisions |  |
|---------|------------|
| forme  | configure la forme de la poignée |
| largeur | largeur de la poignée |
| hauteur | hauteur de la poignée |
| epaisseur b | épaisseur de la pièce B |
| largeur b | largeur de la pièce b |
| espacement | espacement entre la pièce A et la pièce B |

| Fixations |  |
|---------|------------|
| type de fixation | Parker ou M4  |


#### Pour exporter en STL

![](img/02_simulation_false.png)
* décocher la case __simulation__
* cliquer sur la case STL (ou _F7_)

### Exemples 

* poignée carrée en fixation Parker

![](img/03_carre.png)
